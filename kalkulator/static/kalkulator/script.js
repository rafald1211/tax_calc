//javascript 15:11

last_changed_cena_koncowa = 'cenaKoncowa'
last_changed_marza = 'marza'
last_changed_form = null;
cena_bazowa_filled = false;
cena_koncowa_filled = false;
cena_marza_filled = false;
type_tax_filled = false;
type_product_filled = false;


$(".js_tax_type").click(function(){
	
	if(is_cena_bazowa_filled() && is_cena_koncowa_filled() && is_tax_type_selected()){
		console.log('a')
		var amount_tax_type = $(this).attr('value')
		show_info_about_tax(amount_tax_type);
		zaktualizuj_cene_marza()
	}  else if(is_cena_koncowa_filled() && !is_cena_marza_filled() && !is_cena_bazowa_filled()){
		console.log('b')
		var amount_tax_type = $(this).attr('value')
		show_info_about_tax(amount_tax_type);
	}  else if (is_product_type_selected() && is_cena_bazowa_filled() && is_cena_koncowa_filled()){
		console.log('c')
		var amount_tax_type = $(this).attr('value')
		show_info_about_tax(amount_tax_type);
		zaktualizuj_cene_marza();
	} else {
		console.log('d')
		var amount_tax_type = $(this).attr('value')
		show_info_about_tax(amount_tax_type);
		zaktualizuj_cene_koncowa();
	}
});

$(".js_product").click(function(){
	console.log('div product clicked')
	if(is_cena_koncowa_filled() && is_product_type_selected() && is_tax_type_selected() && !is_cena_marza_filled() ||
		(is_cena_koncowa_filled() && is_product_type_selected()) ){
		console.log('11')
		var value = $(this).attr('value')
		$('#id_cena_bazowa').val(value)
		zaktualizuj_cene_marza();
	} else if(is_cena_koncowa_filled() && !is_cena_marza_filled() && !is_tax_type_selected()){
		console.log('22')
		var value = $(this).attr('value')
		$('#id_cena_bazowa').val(value)
	} else{
		console.log('33')
		var value = $(this).attr('value')
		$('#id_cena_bazowa').val(value)
		zaktualizuj_cene_koncowa()	
	}
});

$("#id_cena_marza").change(function() {
	console.log('cena marza changed')
	last_changed_form = last_changed_marza
	what_is_last_changed_form();

	if(is_tax_type_selected()){
		console.log('tax type was SELECTED before marza change')

		if(last_changed_form == last_changed_marza){ // wiec zmieniaj cene koncowa 
			console.log('i zaktualizuj_cene_koncowa')
			zaktualizuj_cene_koncowa()
		} else if(last_changed_form ==last_changed_cena_koncowa){ //wiec zmieniaj marza
			alert('kiedy wystepuje? v1 ')
			console.log('i zaktualizuj_cene_marza')
			zaktualizuj_cene_marza()
		}
	} else{
		alert('select tax type ')
		console.log('tax type NOT selected before marza change')
		$('#id_cena_marza').val('')
	}
});

$("#id_cena_koncowa").change(function(){
	last_changed_form = last_changed_cena_koncowa
	what_is_last_changed_form();
	
	if(last_changed_form == last_changed_marza){ // wiec zmieniaj cene koncowa 
		alert('kiedy wystepuje? v2 ')
		zaktualizuj_cene_koncowa()
	} else if(last_changed_form ==last_changed_cena_koncowa){ //wiec zmieniaj marza
		zaktualizuj_cene_marza()
	}
})


function zaktualizuj_cene_koncowa(){
	console.log('zaktualizuj_cene_koncowa')

	amount_tax_type = get_tax_type()
	cena_bazowa = parseFloat($('#id_cena_bazowa').val())
	cena_marza = parseFloat($('#id_cena_marza').val())
	cena_marza_with_tax = cena_marza*(1-amount_tax_type)
	cena_koncowa = cena_bazowa + cena_marza + (1-amount_tax_type)*cena_marza
	$('#id_cena_koncowa').val(cena_koncowa)

	if(cena_marza_with_tax<0){
		console.log('cena marza with tax mniejsza od 0 ')
		show_extra_info('cena koncowa //twoja marza jest mniejsza niz 0, jestes pewny?')
	}
	else{
		console.log('cena marza with tax wieksza od 0 ')
		show_extra_info('')
	}
}

function zaktualizuj_cene_marza(value){
	console.log('zaktualizuj_cene_marza')
	cena_bazowa = parseFloat($('#id_cena_bazowa').val())
	cena_koncowa = parseFloat($('#id_cena_koncowa').val())
	tax_type = get_tax_type();
	product_amount = get_product_price()
	cena_marza = (cena_koncowa - cena_bazowa)
	final_cena_marza = cena_marza*tax_type
	$('#id_cena_marza').val(final_cena_marza)

	if(final_cena_marza<0){
		show_extra_info('cena marza//twoja marza jest mniejsza niz 0, jestes pewny?')
	}else{
		show_extra_info('')
	}
}


function get_tax_type(){
	if(is_tax_type_selected){
		tax_amount = $('input[name=name_tax_type]:checked').val()
		return tax_amount
	} else{
		alert('select tax type')
	}
}
function get_product_price(){
	if(is_product_type_selected){
		product_price = $('input[name=name_products_type]:checked').val();
		return product_price
	} else{
		alert('select product type')
	}
}

function is_tax_type_selected(){
	if (!$("input[name=name_tax_type]:checked").val()) {
       return false;
    }
    else {
      return true;
    }
}

function is_product_type_selected(){
	if (!$("input[name=name_products_type]:checked").val()) {
       return false;
    }else{
      return true;
    }
}

function is_cena_bazowa_filled(){
	if( $('#id_cena_bazowa').val() == '' ){
		return false;
	}else{
		return true;
	}
}
function is_cena_marza_filled(){
	if( $('#id_cena_marza').val() == '' ){
		return false;
	}else{
		return true;
	}
}
function is_cena_koncowa_filled(){
	if( $('#id_cena_koncowa').val() == '' ){
		return false;
	}else{
		return true;
	}
}

function is_form_filled(){
	if (is_tax_type_selected() && is_product_type_selected()){
		return true;
	} else{
		return false;
	}
}
function show_info_about_tax(tax_amount){
	$('.js_info_for_client').text('Kwota wyświetlana w sklepie to suma ceny bazowej i twojej marży(pomnożona przez '+ tax_amount*100 + '%)')
}

function show_extra_info(text){
	$('.js_extra_info_for_client').text(text);
}
function what_is_last_changed_form(){
	if ( last_changed_form  == last_changed_marza){
		console.log( 'last changed form was marza')
	} else if (last_changed_form == last_changed_cena_koncowa){
		console.log( 'last changed form was cena koncowa')
	}
}