from django.contrib import admin
from .models import Product, TaxType

class ProductAdmin(admin.ModelAdmin):
	list_display = ['product_id', 'product_size', 'product_price', '_has_frame']

 
class TaxTypeAdmin(admin.ModelAdmin): 
	list_display = ['tax_name', 'tax_amount']

admin.site.register(Product, ProductAdmin)
admin.site.register(TaxType, TaxTypeAdmin) 