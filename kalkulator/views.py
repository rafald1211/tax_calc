from django.shortcuts import render
from django.http import HttpResponse
from .models import Product, TaxType
def kalkulator(request):
	products = Product.products.all()
	tax_types = TaxType.list_tax_types.all()
	return render(request, 'kalkulator/index.html', {
		'products':products,
		'tax_types':tax_types} )

# Create your views here
