# -*- coding: utf-8 -*- 
from django.db import models

# class TaxType(models.Model): 
# 	# CONSTANT 
# 	watowiec = 'watowiec' 
# 	nieWatowiec = 'nieWatowiec' 
# 	reszta = 'reszta' 
# 	strWatowiec = 'Jestem watowcem' 
# 	strNieWatowiec = 'Nie jestem watowcem' 
# 	strReszta = 'Nie prowadzę dzialalnosci gospodarczej' 
# 	amountWatowiec = 1 
# 	amountNieWatowiec = 0.82 
# 	amountReszta = 0.7462 
# 	# MODELS 
# 	TAX_OPTION = ( 
# 		(watowiec, strWatowiec), 
# 		(nieWatowiec, strNieWatowiec), 
# 		(reszta, strReszta),#Dla ludzi bez działalności gospodarczej zostaje: Cała kwota - VAT = 0.82%. Podatek dochodowy = 9%(dzialanosc artystyczna) z pozostałego. 0,82*0,09 = 0,7462 
# 	) 

# 	tax_name = models.CharField(max_length = 11, choices = TAX_OPTION, default = strWatowiec) 
# 	tax_amount = models.FloatField(default = amountReszta) 
# 	# tax_amount = 0
# 	def _tax_amount(self): 
# 		print("halko")
# 		if self.tax_name == self.watowiec:
# 			print("wat")
# 			self.tax_amount = self.amountWatowiec
# 		elif self.tax_name ==  self.nieWatowiec:
# 			print("nie wat")
# 			self.tax_amount ==  self.amountNieWatowiec
# 		else:
# 			print("reszta")
# 			self.tax_amount ==  self.amountReszta

# 	def __str__(self): 
# 		if self.tax_name == self.watowiec: 
# 			return 'Jesteś watowcem, na tobie spoczywa obowiązek rozliczenia VAT i odprowadzenia podatku dochodowego' 
# 		elif self.tax_name == self.nieWatowiec: 
# 			return 'Nie jesteś watowcem, otrzymujesz od nas zarobione pieniądze netto, ale spoczywa na tobie obowiązek odprowadzenia podatku dochodowego' 
# 		elif self.tax_name == self.reszta: 
# 			return 'Twoja wypłata to kwota netto pomniejszona o podatek dochodowy, nie martwisz się o podatki - wszystko załatwimy za Ciebie' 

class TaxType(models.Model):
	watowiec = 'watowiec' 
	nieWatowiec = 'nieWatowiec' 
	reszta = 'reszta' 
	strWatowiec = 'Jestem watowcem' 
	strNieWatowiec = 'Nie jestem watowcem' 
	strReszta = 'Nie prowadzę dzialalnosci gospodarczej' 
	amountWatowiec = 1 
	amountNieWatowiec = 0.82 
	amountReszta = 0.7462 

	TAX_OPTION = ( 
		(watowiec, strWatowiec), 
		(nieWatowiec, strNieWatowiec), 
		(reszta, strReszta),#Dla ludzi bez działalności gospodarczej zostaje: Cała kwota - VAT = 0.82%. Podatek dochodowy = 9%(dzialanosc artystyczna) z pozostałego. 0,82*0,09 = 0,7462 
	) 
	TAX_AMOUNT = (
		(watowiec, amountWatowiec),
		(nieWatowiec, amountNieWatowiec),
		(reszta, amountReszta),
	)
	tax_name = models.CharField(max_length = 11, choices = TAX_OPTION, default = strWatowiec) 
	tax_amount = models.FloatField(default = amountReszta) 
	list_tax_types = models.Manager()

	def __str__(self): 
		# return (self.tax_name)
		if self.tax_name == self.watowiec: 
			return ('Jestes watowcem, na tobie spoczywa obowiazek rozliczenia VAT i odprowadzenia podatku dochodowego')
		elif self.tax_name == self.nieWatowiec: 
			return ('Nie jestes watowcem, otrzymujesz od nas zarobione pieniadze netto, ale spoczywa na tobie obowiazek odprowadzenia podatku dochodowego')
		else:
			return ('Twoja wyplata to kwota netto pomniejszona o podatek dochodowy, nie martwisz sie o podatki - wszystko zalatwimy za Ciebie' )


# Create your models here.

class ProductWithFrames(models.Manager):
	def get_queryset(self):
		return super(ProductWithFrames, self).get_queryset().filter(has_frame=True)

class ProductWithoutFrames(models.Manager):
	def get_queryset(self):
		return super(ProductWithoutFrames, self).get_queryset().filter(has_frame=False)

class Product(models.Model):
	frame5050 = '50x50'
	frame5070 = '50x70'
	frame7050 = '70x50'
	PRODUCT_SIZE = (
		(frame5050, '50x50'),
		(frame5070, '50x70'),
		(frame7050, '70x50'),
	)
	product_id = models.IntegerField()
	product_size = models.CharField(max_length = 5, choices = PRODUCT_SIZE, default=frame5050)
	product_price = models.PositiveSmallIntegerField()
	has_frame = models.BooleanField(default = False)

	products = models.Manager()
	products_with_frames = ProductWithFrames()
	products_without_frames = ProductWithoutFrames()

	def _has_frame(self):
		if self.has_frame == True:
			return True
		return False

	def __str__(self):
		if self.has_frame:
			return "Oprawiona rama {} za  {} zl".format(self.product_size, self.product_price)
		else:
			return "Nieoprawiona rama {} za {} zl".format(self.product_size, self.product_price)

