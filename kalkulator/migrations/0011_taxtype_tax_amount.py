# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kalkulator', '0010_remove_taxtype_tax_amount'),
    ]

    operations = [
        migrations.AddField(
            model_name='taxtype',
            name='tax_amount',
            field=models.FloatField(default=0.7462),
        ),
    ]
