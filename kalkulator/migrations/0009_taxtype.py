# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kalkulator', '0008_product_has_frame'),
    ]

    operations = [
        migrations.CreateModel(
            name='TaxType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tax_name', models.CharField(default=b'Jestem watowcem', max_length=11, choices=[(b'watowiec', b'Jestem watowcem'), (b'nieWatowiec', b'Nie jestem watowcem'), (b'reszta', b'Nie prowadz\xc4\x99 dzialalnosci gospodarczej')])),
                ('tax_amount', models.FloatField(default=0.7462)),
            ],
        ),
    ]
