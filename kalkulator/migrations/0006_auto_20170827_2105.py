# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kalkulator', '0005_product_has_frame'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='has_frame',
            field=models.BooleanField(),
        ),
    ]
