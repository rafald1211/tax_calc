# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kalkulator', '0002_product_product_size'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='product_size',
            field=models.CharField(default=b'50x50', max_length=5, choices=[(b'50x50', b'50x50'), (b'50x70', b'50x70'), (b'70x50', b'70x50')]),
        ),
    ]
